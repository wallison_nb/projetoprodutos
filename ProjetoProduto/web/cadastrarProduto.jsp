<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro Produto</title>
    </head>
    <body>
        <h1 align="center">Controle de Produtos</h1>
        <hr>
        <h3 align="center">${mensagem}</h3>
        <form name="cadastrarproduto" action="CadastrarProduto" method="post">
            <table align="center">
                <tr>
                    <th colspan="2">Cadastrar Produtos</th>
                </tr>
                <tr>
                    <td>Descrição:</td>
                    <td><input type="text" name="descricaoProduto"></td>
                </tr>
                <tr>
                    <td>Marca:</td>
                    <td><input type="text" name="marcaProduto"></td>
                </tr>
                <tr>
                    <td>Modelo:</td>
                    <td><input type="text" name="modeloProduto"></td>
                </tr>
                <tr>
                    <td>Preço:</td>
                    <td><input type="text" name="valorProduto"></td>
                </tr>
                <tr>
                    <td>Ano Fabricação:</td>
                    <td><input type="text" name="anoCarro"></td>
                </tr>
                <tr>
                    <td>Modelo Fabricação:</td>
                    <td><input type="text" name="modeloCarro"></td>
                </tr>
                <tr>
                    <td>N° de Portas:</td>
                    <td><input type="text" name="numeroportasCarro"></td>
                </tr>
                <tr>
                    <td>Tipo:</td>
                    <td>
                        <select name="idTipoCarro">
                            <c:forEach var="tipocarro" items="${tiposcarros}">
                                <option value="${tipocarro.idTipoCarro}">
                                    ${tipocarro.nomeTipoCarro}
                                </option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="submit" name="cadastrar" value="Cadastrar"></td>
                </tr>
            </table>
            <h3 align="center"><a href="home.jsp"> Voltar</a></h3>
        </form>
    </body>
</html>
