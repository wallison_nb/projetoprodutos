<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro Produto</title>
    </head>
    <body>
        <h1 align="center">Controle de Produtos</h1>
        <hr>
        <h3 align="center">${mensagem}</h3>
        <form name="cadastrartipocarro" action="CadastrarTipoCarro" method="post">
            <table align="center">
                <tr>
                    <th colspan="2">Cadastrar Tipo Carro</th>
                </tr>
                <tr>
                    <td>Nome:</td>
                    <td><input type="text" name="nome"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="submit" name="cadastrar" value="Cadastrar"></td>
                </tr>
            </table>
            <h3 align="center"><a href="home.jsp"> Voltar</a></h3>
        </form>
    </body>
</html>
