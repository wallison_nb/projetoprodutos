<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alterar Produto</title>
    </head>
    <body>
        <h1 align="center">Controle de Produtos</h1>
        <hr>
        <h3 align="center">${mensagem}</h3>
        <form name="alterarproduto" action="AlterarProduto" method="post">
            <table align="center">
                <tr>
                    <th colspan="2">Alterar Produtos</th>
                </tr>
                <tr>
                    <td>Id:</td>
                    <td><input type="text" name="idProduto" value="${carro.idProduto}" readonly="true"></td>
                </tr>
                <tr>
                    <td>Descrição:</td>
                    <td><input type="text" name="descricaoProduto" value="${carro.descricaoProduto}"></td>
                </tr>
                <tr>
                    <td>Marca:</td>
                    <td><input type="text" name="marcaProduto" value="${carro.marcaProduto}"></td>
                </tr>
                <tr>
                    <td>Modelo:</td>
                    <td><input type="text" name="modeloProduto" value="${carro.modeloProduto}"></td>
                </tr>
                <tr>
                    <td>Preço:</td>
                    <td><input type="text" name="valorProduto" value="${carro.valorProduto}"></td>
                </tr>
                <tr>
                    <td>Ano Fabricação:</td>
                    <td><input type="text" name="anoCarro" value="${carro.anoCarro}"></td>
                </tr>
                <tr>
                    <td>Modelo Fabricação:</td>
                    <td><input type="text" name="modeloCarro" value="${carro.modeloCarro}"></td>
                </tr>
                <tr>
                    <td>N° de Portas:</td>
                    <td><input type="text" name="numeroportasCarro" value="${carro.numeroPortasCarro}"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="submit" name="alterar" value="Alterar"></td>
                </tr>
            </table>
                <h3 align="center"><a href="home.jsp"> Voltar</a></h3>
        </form>
    </body>
</html>
