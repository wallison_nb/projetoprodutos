/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.projetoproduto.dao;

import br.com.projetoproduto.model.Usuario;
import br.com.projetoproduto.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class UsuarioDAOImpl {

    private Connection conn;
    public UsuarioDAOImpl() throws Exception {
        try {
            this.conn = ConnectionFactory.getConnection();
            System.out.println("Conectado com sucesso!");
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }
    
    public Object logarUsuario(String login, String senha) {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Usuario usuario = null;

        String sql = "select * from usuario where loginusuario = ? and senhausuario = ?;";

        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, login);
            stmt.setString(2, senha);
            rs = stmt.executeQuery();

            while (rs.next()) {
                usuario = new Usuario();
                usuario.setNomeUsuario(rs.getString("nomeusuario"));
                usuario.setLoginUsuario(rs.getString("loginusuario"));
                usuario.setSenhaUsuario(rs.getString("senhausuario"));
            }
        } catch (SQLException ex) {
            System.out.println("Problemas ao encontrar usuario! Erro: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                ConnectionFactory.closeConnection(conn, stmt);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar a conexão! Erro: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
        return usuario;
    }
}
