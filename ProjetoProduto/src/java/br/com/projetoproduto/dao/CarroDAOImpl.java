package br.com.projetoproduto.dao;

import br.com.projetoproduto.model.Carro;
import br.com.projetoproduto.model.Produto;
import br.com.projetoproduto.util.ConnectionFactory;
import com.sun.media.sound.DLSModulator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CarroDAOImpl implements GenericDAO {
    
    private Connection conn;

    public CarroDAOImpl() throws Exception {
        try {
            this.conn = ConnectionFactory.getConnection();
            System.out.println("Conectado com sucesso!");
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public Boolean cadastrar(Object object) {
        Carro oCarro = (Carro) object;
        PreparedStatement stmt = null;

        String sql = "insert into carro(anocarro, modelocarro, numeroportascarro, idtipo, idproduto) "
                + "values (?,?,?,?,?);";

        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, oCarro.getAnoCarro());
            stmt.setInt(2, oCarro.getModeloCarro());
            stmt.setInt(3, oCarro.getNumeroPortasCarro());
            stmt.setInt(4, oCarro.getTipocarro().getIdTipoCarro());
            stmt.setInt(5, new ProdutoDAOImpl().cadastrar(oCarro));
            stmt.execute();
            return true;
        } catch (Exception ex) {
            System.out.println("Problemas ao cadastrar carro! Erro: " + ex.getMessage());
            ex.printStackTrace();
            return false;
        } finally {
            try {
                ConnectionFactory.closeConnection(conn, stmt);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar a conexão! Erro:" + ex.getMessage());
                ex.printStackTrace();
            }
        }   
    }

    @Override
    public List<Object> listar() {
        List<Object> carros = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        String sql = "select p.*,c.* from produto p, carro c where p.idproduto = c.idproduto;";
        
        try {
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            
            while (rs.next()) {                
                Carro oCarro = new Carro();
                oCarro.setIdProduto(rs.getInt("idproduto"));
                oCarro.setDescricaoProduto(rs.getString("descproduto"));
                oCarro.setMarcaProduto(rs.getString("marcaproduto"));
                oCarro.setModeloProduto(rs.getString("modeloproduto"));
                oCarro.setValorProduto(rs.getDouble("valorproduto"));
                oCarro.setIdCarro(rs.getInt("idcarro"));
                oCarro.setAnoCarro(rs.getInt("anocarro"));
                oCarro.setModeloCarro(rs.getInt("modelocarro"));
                oCarro.setNumeroPortasCarro(rs.getInt("numeroportascarro"));
                
                carros.add(oCarro);
            }
        } catch (SQLException ex) {
            System.out.println("Problemas ao listar carros! Erro: " +ex.getMessage());
            ex.printStackTrace();
        } finally{
            try {
                ConnectionFactory.closeConnection(conn, stmt, rs);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar conexão! Erro:" +ex.getMessage());
                ex.printStackTrace();
            }
        }
        return carros;
    }

    @Override
    public Boolean excluir(int idObject) {
        PreparedStatement stmt = null;  
        String sql = "Delete from carro where idproduto = ?; commit; delete from produto where idproduto = ?";
        
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, idObject);
            stmt.setInt(2, idObject);
            stmt.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println("Problemas ao excluir carro! Erro: " +ex.getMessage());
            ex.printStackTrace();
            return false;
        }finally{
            try {
                ConnectionFactory.closeConnection(conn, stmt);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar a conexão! Erro: " +ex.getMessage());
                ex.printStackTrace();
            }
        }
        
    }

    @Override
    public Object carregar(int idObject) {  
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        Carro carro = null;
        
        String sql = "Select c.*, p.* from produto p inner join carro c on c.idproduto = p.idproduto where c.idproduto = ?";
        
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, idObject);
            rs = stmt.executeQuery();
            
            while (rs.next()){
                carro = new Carro();
                carro.setIdProduto(rs.getInt("idproduto"));
                carro.setDescricaoProduto(rs.getString("descproduto"));
                carro.setMarcaProduto(rs.getString("marcaproduto"));
                carro.setModeloProduto(rs.getString("modeloproduto"));
                carro.setValorProduto(rs.getDouble("valorproduto"));
                carro.setValorProduto(rs.getDouble("valorproduto"));
                carro.setIdCarro(rs.getInt("idcarro"));
                carro.setAnoCarro(rs.getInt("anocarro"));
                carro.setModeloCarro(rs.getInt("modelocarro"));
                carro.setNumeroPortasCarro(rs.getInt("numeroportascarro"));
            }
        } catch (SQLException ex) {
            System.out.println("Problemas ao carregar carro! Erro: "+ex.getMessage());
            ex.printStackTrace();
        }finally{
            try {
                ConnectionFactory.closeConnection(conn, stmt, rs);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar conexão! Erro: " +ex.getMessage());
                ex.printStackTrace();
            }
        }
        return carro;
    }

    @Override
    public Boolean alterar(Object object) {
        Carro carro = (Carro) object;
        PreparedStatement stmt = null; 
        
        String sql = "update carro set anocarro = ?, modelocarro = ?, numeroportascarro = ? where idproduto = ?";
        
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, carro.getAnoCarro());
            stmt.setInt(2, carro.getModeloCarro());
            stmt.setInt(3, carro.getNumeroPortasCarro());
            stmt.setInt(4, new ProdutoDAOImpl().Alterar(carro));
            stmt.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println("Problemas ao alterar produto! Erro: " +ex.getMessage());
            ex.printStackTrace();
            return false;
        }finally{
            try {
                ConnectionFactory.closeConnection(conn, stmt);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar a conexão! Erro: " +ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

}
