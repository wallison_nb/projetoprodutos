
package br.com.projetoproduto.dao;

import br.com.projetoproduto.model.TipoCarro;
import br.com.projetoproduto.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TipoCarroDAOImpl implements GenericDAO{
    
    private Connection conn;

    public TipoCarroDAOImpl() throws Exception {
        try {
            this.conn = ConnectionFactory.getConnection();
            System.out.println("Conectado com sucesso!");
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public Boolean cadastrar(Object object) {
        TipoCarro oTipoCarro = (TipoCarro) object;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        String sql = "insert into tipocarro(nome) "
                + "values (?);";
        
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, oTipoCarro.getNomeTipoCarro());
            stmt.execute();
            return  true;
        } catch (Exception ex) {
            System.out.println("Problemas ao cadastrar tipo carro! Erro: " +ex.getMessage());
            ex.printStackTrace();
            return false;
        }finally{
            try {
                ConnectionFactory.closeConnection(conn, stmt);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar conexão! Erro: " +ex.getMessage());
                ex.printStackTrace();
            }
        }
            
        
    }

    @Override
    public List<Object> listar() {
        List<Object> tiposcarros = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        String sql = "select * from tipocarro order by nome;";
        
        try {
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            
            while (rs.next()) {                
                TipoCarro oTipoCarro = new TipoCarro();
                oTipoCarro.setIdTipoCarro(rs.getInt("idtipo"));
                oTipoCarro.setNomeTipoCarro(rs.getString("nome"));
                tiposcarros.add(oTipoCarro);
                
            }
        } catch (SQLException ex) {
            System.out.println("Problemas ao listar tipos carros! Erro: " +ex.getMessage());
            ex.printStackTrace();
        } finally{
            try {
                ConnectionFactory.closeConnection(conn, stmt, rs);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar conexão! Erro:" +ex.getMessage());
                ex.printStackTrace();
            }
        }
        return tiposcarros;
    }

    @Override
    public Boolean excluir(int idObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object carregar(int idObject) {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        TipoCarro tipoCarro = null;
        
        String sql = "Select * from tipocarro where idtipo = ?";
        
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, idObject);
            rs = stmt.executeQuery();
            
            while (rs.next()){
                tipoCarro = new TipoCarro();
                tipoCarro.setIdTipoCarro(rs.getInt("idtipo"));
                tipoCarro.setNomeTipoCarro(rs.getString("nome"));
            }
        } catch (SQLException ex) {
            System.out.println("Problemas ao carregar tipo carro! Erro: "+ex.getMessage());
            ex.printStackTrace();
        }finally{
            try {
                ConnectionFactory.closeConnection(conn, stmt, rs);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar conexão! Erro: " +ex.getMessage());
                ex.printStackTrace();
            }
        }
        return tipoCarro;
    }

    @Override
    public Boolean alterar(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
