package br.com.projetoproduto.model;

public class Carro  extends Produto{
    private Integer idCarro;
    private Integer anoCarro;
    private Integer modeloCarro;
    private Integer numeroPortasCarro;
    private TipoCarro tipocarro;

    public Carro() {
    }

    public Carro(Integer idCarro, Integer anoCarro, Integer modeloCarro, Integer numeroPortasCarro, TipoCarro tipocarro) {
        this.idCarro = idCarro;
        this.anoCarro = anoCarro;
        this.modeloCarro = modeloCarro;
        this.numeroPortasCarro = numeroPortasCarro;
        this.tipocarro = tipocarro;
    }

    /**
     * @return the idCarro
     */
    public Integer getIdCarro() {
        return idCarro;
    }

    /**
     * @param idCarro the idCarro to set
     */
    public void setIdCarro(Integer idCarro) {
        this.idCarro = idCarro;
    }

    /**
     * @return the anoCarro
     */
    public Integer getAnoCarro() {
        return anoCarro;
    }

    /**
     * @param anoCarro the anoCarro to set
     */
    public void setAnoCarro(Integer anoCarro) {
        this.anoCarro = anoCarro;
    }

    /**
     * @return the modeloCarro
     */
    public Integer getModeloCarro() {
        return modeloCarro;
    }

    /**
     * @param modeloCarro the modeloCarro to set
     */
    public void setModeloCarro(Integer modeloCarro) {
        this.modeloCarro = modeloCarro;
    }

    /**
     * @return the numeroPortasCarro
     */
    public Integer getNumeroPortasCarro() {
        return numeroPortasCarro;
    }

    /**
     * @param numeroPortasCarro the numeroPortasCarro to set
     */
    public void setNumeroPortasCarro(Integer numeroPortasCarro) {
        this.numeroPortasCarro = numeroPortasCarro;
    }

    /**
     * @return the tipocarro
     */
    public TipoCarro getTipocarro() {
        return tipocarro;
    }

    /**
     * @param tipocarro the tipocarro to set
     */
    public void setTipocarro(TipoCarro tipocarro) {
        this.tipocarro = tipocarro;
    }
    
    

    
    
}
