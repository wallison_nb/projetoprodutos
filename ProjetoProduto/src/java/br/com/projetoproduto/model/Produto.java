package br.com.projetoproduto.model;

public class Produto {
    private Integer idProduto;
    private String descricaoProduto;
    private String marcaProduto;
    private String modeloProduto;
    private Double valorProduto;

    public Produto() {
    }

    //SET atribui, envia os valores
    //GET retorna os valores
    //variavel contante é uma variavel q nunca muda o valor dela. Exemplo equacao de Pi = 3.14159
    public Produto(Integer idProduto, String descricaoProduto, String marcaProduto, String modeloProduto, Double valorProduto) {
        this.idProduto = idProduto;
        this.descricaoProduto = descricaoProduto;
        this.marcaProduto = marcaProduto;
        this.modeloProduto = modeloProduto;
        this.valorProduto = valorProduto;
    }

    /**
     * @return the idProduto
     */
    public Integer getIdProduto() {
        return idProduto;
    }

    /**
     * @param idProduto the idProduto to set
     */
    public void setIdProduto(Integer idProduto) {
        this.idProduto = idProduto;
    }

    /**
     * @return the descricaoProduto
     */
    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    /**
     * @param descricaoProduto the descricaoProduto to set
     */
    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    /**
     * @return the marcaProduto
     */
    public String getMarcaProduto() {
        return marcaProduto;
    }

    /**
     * @param marcaProduto the marcaProduto to set
     */
    public void setMarcaProduto(String marcaProduto) {
        this.marcaProduto = marcaProduto;
    }

    /**
     * @return the modeloProduto
     */
    public String getModeloProduto() {
        return modeloProduto;
    }

    /**
     * @param modeloProduto the modeloProduto to set
     */
    public void setModeloProduto(String modeloProduto) {
        this.modeloProduto = modeloProduto;
    }

    /**
     * @return the valorProduto
     */
    public Double getValorProduto() {
        return valorProduto;
    }

    /**
     * @param valorProduto the valorProduto to set
     */
    public void setValorProduto(Double valorProduto) {
        this.valorProduto = valorProduto;
    }
}
