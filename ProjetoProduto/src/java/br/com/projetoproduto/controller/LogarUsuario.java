/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.projetoproduto.controller;

import br.com.projetoproduto.dao.UsuarioDAOImpl;
import br.com.projetoproduto.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Aluno
 */
@WebServlet(name = "LogarUsuario", urlPatterns = {"/LogarUsuario"})
public class LogarUsuario extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String loginusuario = request.getParameter("loginusuario");
        String senhausuario = request.getParameter("senhausuario");
        String mensagem = null;
        try {

            if (!loginusuario.equals("") && !senhausuario.equals("")) {
                UsuarioDAOImpl dao = new UsuarioDAOImpl();
                Usuario usuario = (Usuario) dao.logarUsuario(loginusuario, senhausuario);

                if (usuario != null) {
                    HttpSession sessao = request.getSession(true);
                    sessao.setAttribute("usuario", usuario);
                    mensagem = "Seja bem-vindo(a) " + usuario.getNomeUsuario();
                    sessao.setAttribute("mensagem", mensagem);
                    request.getRequestDispatcher("home.jsp").forward(request, response);
                } else {
                    mensagem = "Login ou Senha invalidos!";
                    request.setAttribute("mensagem", mensagem);
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                }
            } else {
                mensagem = "É necessario digitar Login e Senha";
                request.setAttribute("mensagem", mensagem);
                request.getRequestDispatcher("index.jsp").forward(request, response);
            }
        } catch (Exception e) {
            System.out.println("Problemas ao logar! Erro: " + e.getMessage());
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
