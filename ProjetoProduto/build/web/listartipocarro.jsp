<%@page import="br.com.projetoproduto.model.TipoCarro"%>
<%@page import="br.com.projetoproduto.model.Carro"%>
<%@page import="br.com.projetoproduto.model.Produto"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Controle de produtos</title>
    </head>
    <body>
        <h1 align="center">Controle de Produtos</h1>
        <hr>
        <h3 align="center">${mensagem}</h3>
        <table align="center" border="1">
            <tr>
                <td colspan="11" align="center">Lista de tipos carros</td>
            </tr>
            <tr>
                <th align="center">Id Tipo carro</th>
                <th align="center">Nome do Tipo do carro</th>
                <th align="center" colspan="2">Editar</th>
            </tr>
            <%
                List<TipoCarro> tiposcarros = (List<TipoCarro>) request.getAttribute("tiposcarros");
                for(TipoCarro tipocarro:tiposcarros){
            %>
            <tr>
                <td align="center"><%=tipocarro.getIdTipoCarro()%></td>
                <td align="center"><%=tipocarro.getNomeTipoCarro()%></td>
                <td align="center"><a href="ExcluirProduto?idProduto=<%=tipocarro.getIdTipoCarro()%>">Excluir</td>
                <td align="center"><a href="CarregarTiposCarros?idProduto=<%=tipocarro.getIdTipoCarro()%>">Alterar</td>
            </tr>
            <% 
                }
            %>
        </table>
        <h3 align="center"><a href="home.jsp"> Voltar</a></h3>
    </body>
</html>
