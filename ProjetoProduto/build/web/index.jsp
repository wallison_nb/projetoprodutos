<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    <body>
        <h1 align="center">Tela de Login</h1>
        <hr>
        <h3 align="center">${mensagem}</h3>
        <form name="logarusuario" action="LogarUsuario" method="post">
            <table align="center">
                <tr>
                    <th colspan="2">Logar</th>
                </tr>
                <tr>
                    <td>Login:</td>
                    <td><input type="text" name="loginusuario"></td>
                </tr>
                <tr>
                    <td>Senha:</td>
                    <td><input type="password" name="senhausuario"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="submit" name="cadastrar" value="Logar"></td>
                </tr>
            </table>
        </form>
    </body>
</html>
